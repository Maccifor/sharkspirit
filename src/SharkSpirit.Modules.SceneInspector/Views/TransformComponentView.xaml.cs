﻿using System.Windows.Controls;

namespace SharkSpirit.Modules.SceneInspector.Views
{
    /// <summary>
    /// Interaction logic for TransformComponentView.xaml
    /// </summary>
    public partial class TransformComponentView
    {
        public TransformComponentView()
        {
            InitializeComponent();
        }
    }
}
